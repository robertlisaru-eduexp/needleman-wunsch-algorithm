package ro.ulbs.bioinformatica.laborator.secvente.aliniere;

import java.security.InvalidParameterException;

public class ScoreMatrix {
    private String topSequence;
    private String leftSequence;
    private Cell scoreMatrix[][];

    public ScoreMatrix(String topSequence, String leftSequence) {
        if (topSequence == null || topSequence.equals("")) {
            throw new InvalidParameterException();
        }
        if (leftSequence == null || leftSequence.equals("")) {
            throw new InvalidParameterException();
        }

        this.topSequence = topSequence;
        this.leftSequence = leftSequence;

        this.scoreMatrix = new Cell[leftSequence.length() + 1][topSequence.length() + 1];
    }

    public void printMatrix() {
        System.out.print("      ");
        for (char c : topSequence.toCharArray()) {
            System.out.print(String.format("%3s", c + " "));
        }
        System.out.println();
        for (int i = 0; i <= leftSequence.length(); i++) {
            if (i > 0) {
                System.out.print(String.format("%3s", leftSequence.charAt(i - 1) + " "));
            } else {
                System.out.print(String.format("%3s", " "));
            }
            for (int j = 0; j <= topSequence.length(); j++) {
                System.out.print(String.format("%3s", scoreMatrix[i][j].getScore() + " "));
            }
            System.out.println();
        }
    }

    public void calculateScores() {
        scoreMatrix[0][0] = new Cell(null, null, null, '0', '0');
        for (int i = 1; i <= topSequence.length(); i++) {
            scoreMatrix[0][i] = new Cell(scoreMatrix[0][i - 1], null, null,
                    topSequence.charAt(i - 1), '0');
        }
        for (int i = 1; i <= leftSequence.length(); i++) {
            scoreMatrix[i][0] = new Cell(null, null, scoreMatrix[i - 1][0], '0',
                    leftSequence.charAt(i - 1));
        }
        for (int i = 1; i <= leftSequence.length(); i++) {
            for (int j = 1; j <= topSequence.length(); j++) {
                scoreMatrix[i][j] = new Cell(scoreMatrix[i][j - 1], scoreMatrix[i - 1][j - 1], scoreMatrix[i - 1][j],
                        topSequence.charAt(j - 1), leftSequence.charAt(i - 1));
            }
        }
    }

    public void solution() {
        traceBack(leftSequence.length(), topSequence.length(), "", "");
    }

    private void traceBack(int i, int j, String topResult, String leftResult) {
        if (i == 0 && j == 0) {
            System.out.println(new StringBuilder(topResult).reverse().toString());
            System.out.println(new StringBuilder(leftResult).reverse().toString());
            System.out.println();
        } else {
            if (scoreMatrix[i][j].isTopArrow()) {
                topResult += "-";
                leftResult += leftSequence.charAt(i - 1);
                traceBack(i - 1, j, topResult, leftResult);
                topResult = topResult.substring(0, topResult.length() - 1);
                leftResult = leftResult.substring(0, leftResult.length() - 1);
            }
            if (scoreMatrix[i][j].isLeftArrow()) {
                leftResult += "-";
                topResult += topSequence.charAt(j - 1);
                traceBack(i, j - 1, topResult, leftResult);
                topResult = topResult.substring(0, topResult.length() - 1);
                leftResult = leftResult.substring(0, leftResult.length() - 1);
            }
            if (scoreMatrix[i][j].isTopLeftArrow()) {
                leftResult += leftSequence.charAt(i - 1);
                topResult += topSequence.charAt(j - 1);
                traceBack(i - 1, j - 1, topResult, leftResult);
            }
        }
    }
}
