package ro.ulbs.bioinformatica.laborator.secvente.aliniere;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try {
            Scanner sc = new Scanner(new File("sequences.in"));
            String topSequence = sc.next();
            String leftSequence = sc.next();
            ScoreMatrix sm = new ScoreMatrix(topSequence, leftSequence);
            sm.calculateScores();
            sm.printMatrix();
            sm.solution();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
        }
    }
}
