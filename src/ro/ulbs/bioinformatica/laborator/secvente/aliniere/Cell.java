package ro.ulbs.bioinformatica.laborator.secvente.aliniere;

public class Cell {
    private int score;
    private boolean topArrow = false;
    private boolean topLeftArrow = false;
    private boolean leftArrow = false;

    public Cell(Cell leftCell, Cell topLeftCell, Cell topCell, char topLetter, char leftLetter) {
        if (topCell == null && leftCell == null) {
            score = 0;
        } else if (topCell == null) {
            score = leftCell.getScore() - 1;
            this.leftArrow = true;
        } else if (leftCell == null) {
            score = topCell.getScore() - 1;
            this.topArrow = true;
        } else {
            int topScore = topCell.getScore() - 1;
            int topLeftScore = topLeftCell.getScore() + (topLetter == leftLetter ? 1 : -1);
            int leftScore = leftCell.getScore() - 1;
            score = Math.max(Math.max(topScore, topLeftScore), leftScore);
            if (topScore == score) {
                this.topArrow = true;
            }
            if (leftScore == score) {
                this.leftArrow = true;
            }
            if (topLeftScore == score) {
                this.topLeftArrow = true;
            }
        }
    }

    public boolean isTopArrow() {
        return topArrow;
    }

    public boolean isTopLeftArrow() {
        return topLeftArrow;
    }

    public boolean isLeftArrow() {
        return leftArrow;
    }

    public int getScore() {

        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
